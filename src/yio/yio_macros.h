/**
 * @file
 * @date 2020-05-12
 * @author Kamil Cukrowski
 * @copyright
 * SPDX-License-Identifier: GPL-3.0-only
 * @brief
 */
#ifndef _yIO_YIO_YIO_MACROS_H_
#define _yIO_YIO_YIO_MACROS_H_

#include "yio_macros_gen.h"

#endif /* _yIO_YIO_YIO_MACROS_H_ */
