/**
 * @file
 * @date 2020-lip-15
 * @author Kamil Cukrowski
 * @copyright
 * SPDX-License-Identifier: GPL-3.0-only
 * @brief
 */
#include <yio.h>

#ifdef _yIO_PRIVATE
#error PRIVATE HEADERS LEAKED TO USERS
#error Regenerate doxygen and see the include graph there
#endif

int main() {}
