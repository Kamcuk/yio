Each file in this directory is templated with characters `π` and `Ω` replaced
by proper replacement dependeing on which implementation is used.

See src/yio/template/README.md for more information.

