/**
 * @file
 * @date 2020-08-12
 * @author Kamil Cukrowski
 * @copyright
 * SPDX-License-Identifier: GPL-3.0-only
 * @brief
 */
#define YIO_PRINT_FLOATS_WITH  YIO_PRINT_FLOATS_WITH_CUSTOM
#include <yπio.h>
#include "test_print_float_on_strto.c"

